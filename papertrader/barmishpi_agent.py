import math
from copy import deepcopy
from agent import Agent


class BarmishPIAgent(Agent):
    """
    An implementation for a discrete-time version of Barmish's PI controller
    for a single security.

    Parameters
    ----------
    security : str
        The ticker of the security on which this agent trades.
    starting_cash : number, def=$100,000
        The starting cash available to this agent.
    starting_inv : number, def=$50,000
        The amount of cash to initially invest in the security (0 will result
        in no trades). Half of starting_cash is a good one. Must be less than
        starting_cash.
    Kp : number
        The Kp constant for tuning the P portion of the PI controller
    Ki : number
        The Ki constant for tuning the I portion of the PI controller
    """

    def __init__(self, security, starting_cash=100000, starting_inv=50000,
                 Kp=2, Ki=2):
        Agent.__init__(self, starting_cash=starting_cash)

        self.Kp = Kp
        self.Ki = Ki

        self.I0 = starting_inv

        self.security = security

        self.sumgt = 0
        self.T = 0

    def first_time(self, day, values):
        """
        Overwrites parent method to move starting_inv out of starting cash
        into security.
        """
        current_price = values[self.security]
        shares = int(round(self.I0 / current_price, 0))
        self.I0 = shares * current_price
        self.curr_portfolio = {
            'Cash': self.starting_cash - self.I0,
            self.security: shares
        }
        self.history[day] = {
            'value': self.starting_cash,
            'portfolio': deepcopy(self.curr_portfolio)
        }

    def decide(self, tdf):
        """
        Implementation of parent method.
        """
        currshares = self.curr_portfolio.get(self.security, 0)
        current_price = tdf.current()[self.security]
        currI = currshares * current_price
        Vt = currI + self.curr_portfolio['Cash']

        # print self.curr_portfolio

        if Vt <= 0:
            self.curr_portfolio['Cash'] = 0
            self.curr_portfolio[self.security] = 0
            return

        gt = Vt - self.starting_cash
        self.sumgt += gt
        self.T += 1

        It = self.I0 + self.Kp * gt + self.Ki * self.sumgt

        desiredshares = int(round(It / current_price, 0))
        deltashares = desiredshares - currshares

        # No leveraging
        maxshares = math.floor(self.curr_portfolio['Cash'] / current_price)
        # 1x shortselling
        maxshort = math.floor(Vt / current_price)

        # print 'Current Shares', currshares
        # print 'Current Price', current_price
        # print 'Current I', currI
        # print 'V(t)', Vt
        # print 'g(t)', gt
        # print 'int g(tau) dtau', self.sumgt
        # print 'T', self.T

        # print 'I(t)', It
        # print 'Desired Shares', desiredshares
        # print 'delta Shares', deltashares

        if deltashares < 0:
            self.sell(self.security, min(-deltashares, maxshort), tdf)
        elif self.curr_portfolio['Cash'] > deltashares * current_price:
            self.buy(self.security, min(deltashares, maxshares), tdf)

        # print self.curr_portfolio
