from copy import deepcopy


class Agent:
    """
    Parent class for agents that trade in the Historic TDF.

    Parameters
    ----------
    starting_cash : positive number, default=100000
        The amount of cash this agent starts with before trading. Defaults to
        $100,000.

    Attributes
    ----------
    curr_portfolio : dict (str -> positive int or positive number)
        A dictionary mapping Yahoo Finance tickers to number of shares owned
        by this agent. If the agent does not own any shares of any given
        ticker (either because the agent never bought any or has since sold
        all of them), then the ticker should not appear in this dictionary.

        One other key in this dictionry is 'Cash', which represents the amount
        of cash owned by the agent. Cash is essentially its own security, where
        quantities are handled by numbers rounded to 2 decimals rather than
        ints. Cash never changes in value over time; therefore the value of
        other securities are implicitely understood to be in terms of cash.
        Also, cash is not directly traded; it increases or decreases as
        other securities are sold or bought respectively.
    history : dict (datetime.date -> dict (str -> object))
        A history of this agent. The keys of the dictionary are the dates of
        each event (e.g. every server tick that this agent has seen). The
        values are dictionaries with the following keys:
            * 'value': The value of the agent's portfolio at this time.
            * 'portfolio': The portfolio owned by the agent at this time; a
              copy of `curr_portfolio` right after the server ticks to this
              time.
    """

    ###########################################################################
    #   Initialization
    ###########################################################################

    def __init__(self, starting_cash=100000):
        self.curr_portfolio = {}
        self.history = {}

        self.starting_cash = starting_cash

    ###########################################################################
    #   Time Actions
    ###########################################################################

    def first_time(self, day, values):
        """
        Initializes the history object, creating the first entry at day `day`.

        Parameters
        ----------
        day : datetime.date
            The day of the first competition tick (the tdf system could tick
            some days that should be used as historical data but not
            registered in the agent's history).
        """
        self.curr_portfolio = {
            'Cash': self.starting_cash
        }
        self.history[day] = {
            'value': self.starting_cash,
            'portfolio': deepcopy(self.curr_portfolio)
        }

    def handle_tick(self, day, values):
        """
        Handles a new tick from TDF, updating the agent's portfolio values
        based on the securities' new values after the tick.

        Parameters
        ----------
        day : datetime.date
            The date wich is now current after the tick has executed.
        values : pandas.Series
            A series where the index are Yahoo Finance ticker symbols and the
            values are the cash values of the respective securities at the
            current day.
        """
        value = 0
        for ticker, quantity in self.curr_portfolio.items():
            if ticker == 'Cash':
                value += quantity
            else:
                value += quantity * values[ticker]
        self.history[day] = {
            'value': value,
            'portfolio': deepcopy(self.curr_portfolio)
        }

    def decide(self, tdf):
        """
        Handle allowing the agent to make a portfolio decision (consisting of
        a set of buys and sells). Called after every tdf tick and after the
        agents have handled the tick.

        This method must be overridden by all child classes.

        Note: it is a good idea to execute all buys before sells in this.

        Parameters
        ----------
        tdf : HistoricTDF
            The tdf instance, allowing the agent to access queries.
        """
        raise RuntimeError('Not implemented')

    ###########################################################################
    #   Portfolio Management
    ###########################################################################

    def buy(self, ticker, shares, tdf):
        """
        Converts cash into quantity `shares` of `ticker`, adding these new
        quantities to the current portfolio.

        Note that a buy does not change the total value of the portfolio; it
        merely changes the composition to a different portfolio of equivalent
        value. The portfolio value itself only changes on a system tick.

        NOTE: it is a good idea to execute all sells before any buys in each
        decision event so that the agent has enough cash.

        Raises
        ------
        Exception
            If the agent does not have enough cash to execute this trade
            (leveraging is disallowed in this version of TDF).

        Parameters
        ----------
        ticker : str
            The security to buy.
        shares : positive int
            The quantity of `ticker` to buy.
        tdf : HistoricTDF
            The TDF instance.
        """
        values = tdf.current()
        cash_needed = values[ticker] * shares

        # print 'Buying %i of %s. Need $%.2f, have $%.2f' % \
        #     (shares, ticker, cash_needed, self.curr_portfolio['Cash'])
        if cash_needed > self.curr_portfolio['Cash']:
            raise Exception('Not enough cash to execute trade.')

        curr_shares = self.curr_portfolio.get(ticker, 0)
        self.curr_portfolio[ticker] = curr_shares + shares
        self.curr_portfolio['Cash'] -= cash_needed

    def sell(self, ticker, shares, tdf):
        """
        Converts quantity `shares` of `ticker` into cash, updating the current
        portfolio.

        Note that a sell does not change the total value of the portfolio; it
        merely changes the composition to a different portfolio of equivalent
        value. The portfolio value itself only changes on a system tick.

        Raises
        ------
        Exception
            If the agent does not own enough shares to sell (short-selling is
            disallowed in this version of TDF).

        Parameters
        ----------
        ticker : str
            The security to sell.
        shares : positive int
            The quantity of `ticker` to sell.
        tdf : HistoricTDF
            The TDF instance.
        """
        values = tdf.current()
        curr_shares = self.curr_portfolio.get(ticker, 0)
        # if shares > curr_shares:
            # raise Exception('Trying to sell too many shares.')
        #   shares = curr_shares

        cash_supplied = values[ticker] * shares
        self.curr_portfolio[ticker] = curr_shares - shares
        if self.curr_portfolio[ticker] == 0:
            self.curr_portfolio.pop(ticker, None)
        self.curr_portfolio['Cash'] += cash_supplied

        # print 'Selling %i of %s (have %i). $%.2f supplied to give $%.2f' % \
        #     (shares, ticker, self.curr_portfolio[ticker], cash_supplied,
        #      self.curr_portfolio['Cash'])
