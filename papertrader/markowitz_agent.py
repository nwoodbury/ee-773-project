# import tdfTrader as trader
import pandas as pd
import math
import pulp

from agent import Agent

# TDF Information
host = 'http://localhost:3000'
agentid = '5334c4a06b983ef57102948b'
apikey = 'qmemevlbyesdhbciwjshvaswloeqybrn'
symbols = ['AAPL', 'MSFT', 'YHOO', 'MU',
           'XOM', 'GE', 'CVX',
           'C', 'BAC', 'JPM']

torun = 'tdf-static'


class MarkowitzAgent(Agent):
    """
    The agent running the Markowitz (actually MAD) algorithms.

    Parameters
    ----------
    starting_cash : number
        Inherited from agent
    window : non-negative int, default=100
        The number of ticked-days into the past this agent will look to make
        predictions.
    mu : non-negative number
        The risk-adversion parameter
    """

    def __init__(self, starting_cash=100000, window=100, mu=1):
        Agent.__init__(self)
        self.window = window
        self.mu = mu

    def decide(self, tdf):
        """
        Implementation of parent's decider.
        """
        # Step 1: Collect Stock Data
        returns, means, stdevs, histories = self.get_stock_data(
            src='tdf', windowdata=tdf.last(self.window))

        # Step 2: Formulate the MAD linear program
        problem, x, y = self.get_mad_lp(returns, means, self.mu, lb=0.0)

        # Step 3: Solve the MAD LP adn collect results
        desired_comp = self.solve_problem(problem, x)

        # Step 4: Compute difference between desired and current portfolio
        curr_vals = tdf.current()
        curr_comp, value = self.compute_composition(self.curr_portfolio,
                                                    curr_vals)
        delta_comp = self.get_composition_change_percent(desired_comp,
                                                         curr_comp,
                                                         tdf.symbols)
        delta_shares = self.convert_to_shares(delta_comp, curr_vals,
                                              value)

        # print 'Current Values:\n', curr_vals
        # print 'Desired Composition', desired_comp
        # print 'Sum composition', sum(desired_comp.values())
        # print 'Current Portfolio', self.curr_portfolio
        # print 'Current Composition', curr_comp
        # print 'Portfolio Value', value
        # print 'Delta Composition', delta_comp
        # print 'Delta Shares', delta_shares

        # Step 5: Execute Trades (sell before buy)
        for security, quantity in delta_shares.items():
            # sell
            if quantity < 0:
                self.sell(security, -quantity, tdf)
        for security, quantity in delta_shares.items():
            # buy
            if quantity > 0:
                self.buy(security, quantity, tdf)

        final_comp, final_val = self.compute_composition(self.curr_portfolio,
                                                         curr_vals)
        # print 'Final Shares', self.curr_portfolio
        # print 'Final Value', final_val
        # print 'Final Composition', final_comp
        assert round(final_val, 2) == round(value, 2)



    def __ap_row(self, x):
        """
        Utility used to convert the all histories to a dataframe of bid prices.

        Parameters
        ----------
        x : pandas.Series
            A row in the DataFrame retrieved by an AllHistories query.

        Returns
        -------
        row : pandas.Series
            The row where each element contains only the bid price.
        """
        return x.apply(lambda y: y['bid'])

    def stdev(self, s, means):
        """
        Used for computing the standard deviation for a column of time-series
        returns for a particular series.

        Parameters
        ----------
        s : pandas.Series
            The column for a single security showing the returns of that
            security over time.
        means : pandas.Series
            A series of the mean of returns for all securities.unit test

        Returns
        -------
        standard_deviation : number
            The standard deviation of the returns for the given security.
        """
        mean = means[s.name]
        summed = 0
        count = 0
        for rtn in s:
            summed = (rtn - mean) ** 2
            count += 1

        return math.sqrt(summed / count)

    def get_stock_data(self, src='tdf', windowdata=None):
        """
        Given a list of symbols, queries TDF for the historical prices of those
        symbols, returning data for the historical returns, means of returns,
        and standard deviation of returns for each security.

        Parameters
        ----------
        src : str in {'tdf', 'test'}
            The source of the data. If 'tdf', grabs historical prices from the
            Historic TDF system. If 'test', grabs data from the CS 412 finance
            homework problem.

        Returns
        -------
        returns : pandas.DataFrame
            A dataframe where the columns are symbols and each row is the
            return seen at a particular time indicated by the row. Sorted from
            oldest to most recent.
        means : pandas.Series
            A series where each label is a symbol and the value is the mean of
            returns seen by that security.
        stdevs : pandas.Series
            A series where each label is a symbol and the value is the standard
            deviation of returns seen by that security.
        histories : pandas.DataFrame
            Similar to returns, but with stock prices instead of returns for
            values.
        """
        if src == 'tdf' or src == 'tdf-static':
            histories = windowdata
            # if src == 'tdf':
            #     histories = pd.DataFrame(trader.allHistories(host))
            #     histories = histories.transpose()[symbols]
            #     histories = histories.apply(lambda x: __ap_row(x), axis=1)
            # else:
            #     histories = pd.read_csv('biddata.csv', parse_dates=True,
            #                             index_col=0)
            #
            curr = histories[1:]
            prev = histories[:-1]

            prev = prev.transpose()
            prev.columns = list(curr.axes[0])
            prev = prev.transpose()

            returns = (curr - prev) / prev
        elif src == 'test':
            returns = pd.read_csv('testdata.csv')
            histories = None

        means = returns.mean()

        stdevs = returns.apply(lambda x: self.stdev(x, means), axis=0)

        return [returns, means, stdevs, histories]

    def get_mad_lp(self, returns, means, mu, lb=0):
        """
        Formulates the MAD model as a pulp linear program.

        Parameters
        ----------
        returns : pandas.DataFrame
            A dataframe where the columns are symbols and each row is the
            return seen at a particular time indicated by the row. Sorted from
            oldest to most recent.
        means : pandas.Series
            A series where each label is a symbol and the value is the mean of
            returns seen by that security.
        mu : number
            The risk aversion factor.
        lb : number 0 <= x < 1
            The percentage of the portfolio that must be kept in cash.

        Returns
        -------
        problem : pulp.LpProblem
            The pulp lp problem with variables, an objective, and constraints
            added.
        """
        problem = pulp.LpProblem('Markowitz Portfolio Optimization',
                                 pulp.LpMaximize)
        symbols = list(returns.axes[1])
        x = pulp.LpVariable.dicts('x', symbols, lowBound=0)
        y = pulp.LpVariable.dicts('y', range(0, len(returns.axes[0])),
                                  lowBound=0)

        # Objective: Max sum_i (mean(i)*x_i) - mu/float T * sum_j y_j
        T = float(len(y))
        mudT = mu / T
        problem += (sum(means[i] * x[i] for i in x) -
                    sum(y[j] * mudT for j in y)), 'objective'

        # Equality constraint: sum_i x_i = 1
        problem += sum(x[i] for i in x) == 1 - lb

        # Time constraint: sum_j (mean(i) - return(i, j))x_i <= y_j for all j
        for j in range(0, len(y)):
            row = returns.iloc[j]
            problem += sum((means[i] - row[i]) * x[i] for i in x) <= y[j]
            # problem += sum((means[i] - row[i]) * x[i] for i in x) >= -y[j]

        return problem, x, y

    def solve_problem(self, problem, x):
        """
        Solves the given lp problem, returning the selected optimal portfolio.
        """
        problem.solve()
        portfolio = {i: x[i].value() for i in x}
        return portfolio

    def fetch_current_status(self):
        """
        Fetches and returns the agent's status from TDF.

        OBSOLETE
        """
        raise NotImplemented('TDF')
        # status = trader.status(host, agentid, apikey)
        # return status

    def compute_composition(self, portfolio, values):
        """
        Converts shares to percentages of total value for each security.

        Parameters
        ----------
        values : pandas.Series
            The current values of securities
        """
        value = 0
        for security, quantity in portfolio.items():
            if security == 'Cash':
                value += quantity
            else:
                value += quantity * values[security]

        curr_comp = {}
        for security, quantity in portfolio.items():
            if security == 'Cash':
                continue
            curr_comp[security] = values[security] * quantity / float(value)
        return curr_comp, value

    def get_composition_change_percent(self, desired_comp, curr_composition,
                                       symbols):
        """
        Computes the change in computation from present to the computed optimal
        (in percent portfolio value).

        Parameters
        ----------
        portfolio : dict
            The computed optimal portfolio.
        curr_composition : dict
            The current portfolio.

        Returns
        -------
        delta_portfolio : dict
            The change in portfolio percent.
        """
        delta_portfolio = {}
        for security in symbols:
            desired = desired_comp.get(security, 0)
            current = curr_composition.get(security, 0)
            delta_portfolio[security] = desired - current
        return delta_portfolio

    def convert_to_shares(self,  delta_comp, values, total_value):
        """
        Converts a portfolio of percentages to a number of shares of each
        security in the portfolio that should be bought (+) or sold (-).

        Parameters
        ----------
        portfolio : dict
            The computed optimal portfolio.
        values : pandas.Series
            The present ask values of all securities.
        portfolio_value : number
            The present value of the portfolio.
        lb : number
            The percent of the portfolio that should remain as cash

        Returns
        -------
        shares : pandas.Series
            The shares of each security that should be owned.
        """
        shares = {}
        for security, percent in delta_comp.items():
            if percent <= 0:
                op = math.floor
            else:
                op = math.floor
            shares[security] = op(percent * total_value / values[security])
        return shares
