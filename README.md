## Installation ##

To install the project and its dependencies, simply:

        sudo apt-get install python-matplotlib python-pandas
        python setup.py develop --user

If the ipython notebooks are to be utilized (recommended but not required),
install `ipython` and `ipython-notebook` by:

        sudo apt-get install ipython ipython-notebook

## Testing ##

To run the test cases:

        python setup.py test

## Running the Ipython Notebooks ##

Ipython notebooks have been created to make the execution of the trading system
easier. In order to use these notebooks, `ipython` and `ipython-notebook` must
have been previously installed (see the Installation section above).

To run the notebooks, navigate to the `notebooks/` directory and execute the
following command:

        ipython notebook --pylab=inline

This command will start a local server and the ipython notebook page will open
in the default browser. This page will list all available notebooks in the
directory `notebooks/`. Click on any of them, and their descriptions and
instructions on how to use them will be posted to the top of the notebook.
