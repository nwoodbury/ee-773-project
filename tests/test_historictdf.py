import os
import shutil
import math

import pytest
import pandas as pd
from datetime import date

from papertrader.historic_tdf import HistoricTDF
from papertrader.agent import Agent

TESTSYMBOLS = ['MSFT', 'MU']
DIR = 'testdata/'


def trim_historic(tdf):
    """
    Shortens historic in tdf to contain only data to contain only one week
    in April 2015 (4/13/15 - 4/17/15).

    !! Do not call after the tdf instance has ticked, otherwise strange things
    can happen.
    """
    data = tdf.historic
    data = data['20150413':'20150417']
    tdf.historic = data
    return tdf


def assert_sorted_index(historic):
    last = None
    for ix in list(historic.axes[0]):
        print ix
        if last is not None:
            assert ix > last
        last = ix


class TestHistoricTDFInit(object):

    def test_nocache_noforce(self):
        if os.path.exists(os.path.dirname(DIR)):
            shutil.rmtree(DIR)

        tdf = HistoricTDF(symbols=TESTSYMBOLS)
        tdf.collect_data(data_loc=DIR)
        assert len(tdf.historic.columns) == 2

        tdf = trim_historic(tdf)
        assert len(tdf.historic) == 5

        assert_sorted_index(tdf.historic)

    def test_cache_noforce(self):
        tdf1 = HistoricTDF(symbols=TESTSYMBOLS)
        tdf1.collect_data(data_loc=DIR)

        tdf = HistoricTDF(symbols=TESTSYMBOLS)
        tdf.collect_data(data_loc=DIR)
        assert len(tdf.historic.columns) == 2

        tdf = trim_historic(tdf)
        assert len(tdf.historic) == 5

        assert_sorted_index(tdf.historic)

    def test_cache_force(self):
        tdf1 = HistoricTDF(symbols=TESTSYMBOLS)
        tdf1.collect_data(data_loc=DIR)

        # clear csv data, which would cause an error if cache is not refreshed
        msft = pd.read_csv('%sMSFT.csv' % DIR, parse_dates=[0],
                           index_col=0)
        msft = msft['20160101':'20160110']
        msft.to_csv('%sMSFT.csv' % DIR)
        merged = pd.read_csv('%smerged.csv' % DIR, parse_dates=[0],
                             index_col=0)
        merged = merged['20160101':'20160110']
        merged.to_csv('%smerged.csv' % DIR)

        tdf = HistoricTDF(symbols=TESTSYMBOLS)
        tdf.collect_data(force_regenerate=True, data_loc=DIR)
        assert len(tdf.historic.columns) == 2

        tdf = trim_historic(tdf)
        assert len(tdf.historic) == 5

        assert_sorted_index(tdf.historic)


class TestHistoricTDF(object):

    def setup(self):
        tdf = HistoricTDF(symbols=TESTSYMBOLS)
        tdf.collect_data(data_loc=DIR)
        self.tdf = trim_historic(tdf)
        self.tdf.finalize_init()

    def test_no_tick(self):
        assert self.tdf.is_initialized

        assert self.tdf.today == date(2015, 4, 13)

        last1 = self.tdf.last(1)
        assert len(last1) == 1
        assert HistoricTDF.date_at_index(last1, 0) == date(2015, 4, 13)

        last2 = self.tdf.last(2)
        assert len(last2) == 1
        assert HistoricTDF.date_at_index(last2, 0) == date(2015, 4, 13)

    def test_one_tick(self):
        assert self.tdf.today == date(2015, 4, 13)

        self.tdf.tick()
        assert self.tdf.today == date(2015, 4, 14)

        last1 = self.tdf.last(1)
        assert len(last1) == 1
        assert HistoricTDF.date_at_index(last1, 0) == date(2015, 4, 14)

        last2 = self.tdf.last(2)
        assert len(last2) == 2
        assert HistoricTDF.date_at_index(last2, 0) == date(2015, 4, 13)
        assert HistoricTDF.date_at_index(last2, 1) == date(2015, 4, 14)

        last3 = self.tdf.last(3)
        assert len(last3) == 2
        assert HistoricTDF.date_at_index(last3, 0) == date(2015, 4, 13)
        assert HistoricTDF.date_at_index(last3, 1) == date(2015, 4, 14)

    def test_two_ticks(self):
        assert self.tdf.today == date(2015, 4, 13)

        self.tdf.tick()
        self.tdf.tick()
        assert self.tdf.today == date(2015, 4, 15)

        last1 = self.tdf.last(1)
        assert len(last1) == 1
        assert HistoricTDF.date_at_index(last1, 0) == date(2015, 4, 15)

        last2 = self.tdf.last(2)
        assert len(last2) == 2
        assert HistoricTDF.date_at_index(last2, 0) == date(2015, 4, 14)
        assert HistoricTDF.date_at_index(last2, 1) == date(2015, 4, 15)

        last3 = self.tdf.last(3)
        assert len(last3) == 3
        assert HistoricTDF.date_at_index(last3, 0) == date(2015, 4, 13)
        assert HistoricTDF.date_at_index(last3, 1) == date(2015, 4, 14)
        assert HistoricTDF.date_at_index(last3, 2) == date(2015, 4, 15)

        last4 = self.tdf.last(4)
        assert len(last4) == 3
        assert HistoricTDF.date_at_index(last4, 0) == date(2015, 4, 13)
        assert HistoricTDF.date_at_index(last4, 1) == date(2015, 4, 14)
        assert HistoricTDF.date_at_index(last4, 2) == date(2015, 4, 15)

        self.tdf.tick(2)
        assert self.tdf.today == date(2015, 4, 17)

    def test_toomany_ticks(self):
        assert self.tdf.today == date(2015, 4, 13)

        with pytest.raises(Exception):
            self.tdf.tick(6)
            print self.tdf.today


class TestTDFWithBaseAgent(object):

    def test_base_agent(self):
        agent = Agent()

        tdf = HistoricTDF(agents={'agent': agent}, symbols=TESTSYMBOLS)
        tdf.collect_data(data_loc=DIR)
        tdf = trim_historic(tdf)
        tdf.finalize_init()

        with pytest.raises(RuntimeError):
            agent.decide(tdf)
        with pytest.raises(RuntimeError):
            tdf.tick()


class AgentTester(Agent):

    def __init__(self):
        Agent.__init__(self)
        self.ticks = 0

    def decide(self, tdf):
        self.ticks += 1

        if self.ticks == 1:
            pass
        elif self.ticks == 2:
            self.buy('MSFT', 300, tdf)
        elif self.ticks == 3:
            self.sell('MSFT', 150, tdf)
            self.buy('MU', 500, tdf)
        elif self.ticks == 4:
            self.sell('MSFT', 150, tdf)
            self.sell('MU', 500, tdf)
        else:
            raise Exception('Too many ticks')


class TestTDFWithAgentTester(object):

    def setup(self):
        self.agent = AgentTester()

        tdf = HistoricTDF(agents={'agent': self.agent}, symbols=TESTSYMBOLS)
        tdf.collect_data(data_loc=DIR)
        self.tdf = trim_historic(tdf)
        # self.tdf.historic.to_csv('historic.csv')
        tdf.finalize_init()

    def test_before_tick(self):
        day = date(2015, 4, 13)
        _assert_history({
            day: {
                'value': 100000,
                'portfolio': {'Cash': 100000}
            }
        }, self.agent.history)
        _assert_portfolio({'Cash': 100000}, self.agent.curr_portfolio)

    def test_onetick_noaction(self):
        self.tdf.tick()

        day1 = date(2015, 4, 13)
        day2 = date(2015, 4, 14)

        base_day = {
            'value': 100000,
            'portfolio': {'Cash': 100000}
        }
        _assert_history({day1: base_day, day2: base_day},
                        self.agent.history)
        _assert_portfolio({'Cash': 100000}, self.agent.curr_portfolio)

    def test_twotick_buy(self):
        self.tdf.tick()
        self.tdf.tick()

        day1 = date(2015, 4, 13)
        day2 = date(2015, 4, 14)
        day3 = date(2015, 4, 15)

        base_day = {
            'value': 100000,
            'portfolio': {'Cash': 100000}
        }
        curr_day = {
            'Cash': 87322,
            'MSFT': 300
        }
        _assert_history({
            day1: base_day,
            day2: base_day,
            day3: base_day,  # {'value': 100000, 'portfolio': curr_day}
        }, self.agent.history)
        _assert_portfolio(curr_day, self.agent.curr_portfolio)

    def test_threetick_sellbuy(self):
        self.tdf.tick()
        self.tdf.tick()
        self.tdf.tick()

        day1 = date(2015, 4, 13)
        day2 = date(2015, 4, 14)
        day3 = date(2015, 4, 15)
        day4 = date(2015, 4, 16)

        base_day = {
            'value': 100000,
            'portfolio': {'Cash': 100000}
        }
        _assert_history({
            day1: base_day,
            day2: base_day,
            day3: base_day,
            day4: {
                'value': 99970,
                'portfolio': {'Cash': 87322, 'MSFT': 300}
            }
        }, self.agent.history)
        _assert_portfolio({
            'Cash': 79641,
            'MSFT': 150,
            'MU': 500
        }, self.agent.curr_portfolio)

    def test_fourtick_sellall(self):
        self.tdf.tick()
        self.tdf.tick()
        self.tdf.tick()
        self.tdf.tick()

        day1 = date(2015, 4, 13)
        day2 = date(2015, 4, 14)
        day3 = date(2015, 4, 15)
        day4 = date(2015, 4, 16)
        day5 = date(2015, 4, 17)

        base_day = {
            'value': 100000,
            'portfolio': {'Cash': 100000}
        }
        _assert_history({
            day1: base_day,
            day2: base_day,
            day3: base_day,
            day4: {
                'value': 99970,
                'portfolio': {'Cash': 87322, 'MSFT': 300}
            },
            day5: {
                'value': 99894,
                'portfolio': {'Cash': 79641, 'MSFT': 150, 'MU': 500}
            }
        }, self.agent.history)
        _assert_portfolio({
            'Cash': 99894
        }, self.agent.curr_portfolio)


def assert_almost(left, right, tol=0.01):
    assert math.fabs(left - right) <= tol


def _assert_history(expected, actual):
    assert len(expected) == len(actual)
    for day, fullportfolio in expected.items():
        assert day in actual
        actfullportfolio = actual[day]

        assert 'portfolio' in actfullportfolio
        assert 'value' in actfullportfolio

        assert_almost(fullportfolio['value'], actfullportfolio['value'])

        portfolio = fullportfolio['portfolio']
        actportfolio = fullportfolio['portfolio']
        _assert_portfolio(portfolio, actportfolio)


def _assert_portfolio(portfolio, actportfolio):
    assert len(portfolio) == len(actportfolio)

    for ticker, quantity in portfolio.items():
        assert ticker in actportfolio
        assert_almost(quantity, actportfolio[ticker])
