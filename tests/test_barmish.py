from datetime import date

from papertrader.historic_tdf import HistoricTDF
from papertrader.barmishpi_agent import BarmishPIAgent

from test_historictdf import DIR, TESTSYMBOLS, trim_historic
from test_historictdf import _assert_portfolio, _assert_history


class TestBarmish(object):

    def setup(self):
        self.agent = BarmishPIAgent('MSFT')

        tdf = HistoricTDF(agents={'agent': self.agent}, symbols=TESTSYMBOLS)
        tdf.collect_data(data_loc=DIR)
        self.tdf = trim_historic(tdf)
        tdf.finalize_init()

    def test_before_tick(self):
        day = date(2015, 4, 13)
        curr_portfolio = {'Cash': 50013.28, 'MSFT': 1197}

        _assert_history({
            day: {
                'value': 100000,
                'portfolio': curr_portfolio
            }
        }, self.agent.history)
        _assert_portfolio(curr_portfolio, self.agent.curr_portfolio)

    def test_one_tick(self):
        self.tdf.tick()

        day1 = date(2015, 4, 13)
        day2 = date(2015, 4, 14)

        _assert_history({
            day1: {
                'value': 100000,
                'portfolio': {'Cash': 50013.28, 'MSFT': 1197}
            },
            day2: {
                'value': 99868.33,
                'portfolio': {'Cash': 50013.28, 'MSFT': 1197}
            }
        }, self.agent.history)
        _assert_portfolio({
            'Cash': 50388.13, 'MSFT': 1188
        }, self.agent.curr_portfolio)

    def test_two_ticks(self):
        self.tdf.tick()
        self.tdf.tick()

        day1 = date(2015, 4, 13)
        day2 = date(2015, 4, 14)
        day3 = date(2015, 4, 15)

        _assert_history({
            day1: {
                'value': 100000,
                'portfolio': {'Cash': 50013.28, 'MSFT': 1197}
            },
            day2: {
                'value': 99868.33,
                'portfolio': {'Cash': 50013.28, 'MSFT': 1197}
            },
            day3: {
                'value': 100593.01,
                'portfolio': {'Cash': 48528.69, 'MSFT': 1233}
            }
        }, self.agent.history)
